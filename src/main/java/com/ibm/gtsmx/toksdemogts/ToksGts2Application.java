// Added comment
// Other comment and more comments
// Test automated build and CI tests - . trigger CI build and tests. 

package com.ibm.gtsmx.toksdemogts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ToksGts2Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ToksGts2Application.class);
	}
	 
	public static void main(String[] args) {
		SpringApplication.run(ToksGts2Application.class, args);
	}

}
